# README #

Bu uygulama https://github.com/DaftMonk/generator-angular-fullstack Yeoman generator'ını kullanmaktadır.



### How do I get set up? ###

* Repo kopyalandıktan sonra proje klasöründe **npm install** & **bower install** komtları ile gerekliliklerin yüklenmesi gerekiyor.
* Gereklilikler yüklendikten sonra **grunt serve** komut ile proje lokalde çalıştırılabiir.
* grunt derve:dist veya **grunt build** ile proje build edilip production çıktısı alınabilir.
* Oluşan dist klasöründe heroku reposuna deploy yapılıp uygulama herokuda çalıştırılabilir.