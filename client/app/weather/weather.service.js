'use strict';

(function(){
	var injectParams = ['$http'];
  var weatherFactory = function($http){
    var weatherFactory = {};
    weatherFactory.getAllCities = function(){
      return $http.get('app/cities.json', { cache: true}); //, {headers: {'Content-Type': 'application/javascript'}}
    };
    weatherFactory.pullForecast = function(cityName){
			return $http.get('http://api.openweathermap.org/data/2.5/forecast/daily?q='+cityName+',tr&units=metric&cnt=3&APPID=d54179f10fb99d4e46a267dd9b97a445');
    };
	  return weatherFactory;
  };
	weatherFactory.$inject = injectParams;
    angular.module('weatherApp').factory('weatherFactory', weatherFactory);
})();
