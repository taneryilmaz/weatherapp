'use strict';
(function(){
  var injectParams = ['$scope', '$mdToast', 'weatherFactory'];

  function WeatherCtrl($scope, $mdToast, weatherFactory){
    $scope.isProgress = false;
    $scope.forecasts  = [];
    $scope.cities     = [];

    weatherFactory.getAllCities().success(function(response){
      $scope.allCities = response;
    });

    $scope.pullForecast = function(cityItem){
      $scope.isProgress = true;
      $scope.cities.push(cityItem);
      weatherFactory.pullForecast(cityItem.display).success(function(response){
        console.log(response)
        $scope.forecasts.push(response);
        $scope.$broadcast('forecastAdded', $scope.forecasts);
        $scope.isProgress = false;
      }).error(function(response){
        var index = _.findIndex($scope.cities, cityItem);
        $scope.$broadcast('removeThisTag', index);
        $mdToast.show( $mdToast.simple().content('Hava durumu bilgisi alınamadı daha sonra tekrar deneyin.').position('top right').hideDelay(4000) );
        $scope.isProgress = false;
        $scope.cities.splice(index, 1);
      });
    };

    $scope.removeForecast = function(cityItem){
      var index = _.findIndex($scope.cities, cityItem);
      if(index === -1){return;}
      $scope.cities.splice(index, 1);
      $scope.forecasts.splice(index, 1);
    };

  };
  WeatherCtrl.$inject = injectParams;
    angular.module('weatherApp').controller('WeatherCtrl', WeatherCtrl);
})();
