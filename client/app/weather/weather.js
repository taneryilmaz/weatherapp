'use strict';

angular.module('weatherApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('weather', {
        url: '/',
        templateUrl: 'app/weather/weather.html',
        controller: 'WeatherCtrl'
      });
  });
