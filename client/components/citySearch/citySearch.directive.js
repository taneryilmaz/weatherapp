'use strict';

(function(){
  var injectParams = ['$mdToast', '$q', '$http', '$timeout'];
	var citySearch = function($mdToast, $q, $http, $timeout) {
	  return {
	    restrict: 'EA',
      scope: {
        allCities    : '='
      },
      templateUrl: 'components/citySearch/citySearch.html',
	    link: function ($scope, $element, $attrs) {
        var cityCounter = 0,
            tagList;

        /**
          * ng-tags-input expected promise
        */
        var deferred = function(){
          var deferred = $q.defer();
          deferred.resolve($scope.allCities);
          return deferred.promise;
        };
        /**
        * Give all tags ng-tags-input
        */
        $scope.loadTags = function($query){
          return deferred().then(function(response){
             return response.filter(function(city) {
              return city.display.toLowerCase().indexOf($query.toLowerCase()) != -1;
             });
          });
        };

        $scope.removeTag = function(index){
          $timeout(function(){
            tagList = document.getElementsByTagName("md-icon");
            console.log(tagList, index);
            tagList[index].click();
          }, 0);
        };
        $scope.$parent.$on('removeThisTag', function(e, tag){
          $scope.removeTag(tag);
        });

        $scope.tagAdded = function(tag){
          cityCounter++;
          if(cityCounter === 6){
            $mdToast.show( $mdToast.simple().content('Maksimum 5 şehir seçebilirsiniz').position('top right').hideDelay(5000) );
            $scope.removeTag(5);
            return;
          }
          $scope.$parent.pullForecast(tag);
        };

        $scope.tagRemoved = function(tag){
          cityCounter--;
          $scope.$parent.removeForecast(tag);
        };

	    }
	  };
	};
  citySearch.$inject = injectParams;
	angular
		.module('weatherApp')
		.directive('citySearch', citySearch);
})();
