'use strict';

describe('Directive: citySearch', function () {

  // load the directive's module and view
  beforeEach(module('weatherAppApp'));
  beforeEach(module('components/citySearch/citySearch.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<city-search></city-search>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the citySearch directive');
  }));
});