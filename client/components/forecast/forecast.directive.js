'use strict';

(function(){
  var injectParams = [];
	var forecasts = function() {
	  return {
	    restrict: 'EA',
      scope: {},
      templateUrl: 'components/forecast/forecast.html',
	    link: function ($scope, $element, $attrs, $ctrl) {
        $scope.now = new Date().getTime();

        $scope.$parent.$on('forecastAdded', function(e, forecasts){
          console.log('forecastAdded', forecasts);
          $scope.forecastItems = forecasts;
        });

      }
	  };
	};
  forecasts.$inject = injectParams;
	angular
		.module('weatherApp')
		.directive('forecasts', forecasts);
})();
