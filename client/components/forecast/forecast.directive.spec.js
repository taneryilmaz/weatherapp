'use strict';

describe('Directive: forecast', function () {

  // load the directive's module and view
  beforeEach(module('weatherAppApp'));
  beforeEach(module('components/forecast/forecast.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<forecast></forecast>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the forecast directive');
  }));
});